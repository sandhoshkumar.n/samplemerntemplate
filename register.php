<?php 
header("Content-Type: application/json");
$name = isset($_GET['name']) ? $_GET['name'] : '';
$res_arr = array();
//check only contains letters
if (ctype_alpha($name)) {
	//return success with message
	$res_arr = array(
		'error' => array(),
		'success' => array('message'=> "The username consists of all letters.")
	);
} else {
	//return error with message
	$res_arr = array(
		'error' => array('message'=> "The username does not consist of all letters."),
		'success' => array()
	);
}
echo json_encode($res_arr);
?>